// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
let parentDivTag;
let form;
window.onload = function(){
    //let submitButton = document.getElementById('submit');
    //submitButton.addEventListener('click', getArticles);
    parentDivTag = document.getElementById('books-container');
    form = document.getElementsByTagName('form')[0];
    form.addEventListener("submit", getBooks, false)
};

const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
const BASE_URL1 = 'https://api.nytimes.com/svc/books/v3/lists/';
const BASE_URL2 = '/hardcover-fiction.json?api-key=';
//const url = `${BASE_URL}?q=cars&api-key=${API_KEY}`;


let buildUrl = function(){
    let year = document.getElementsByName('year')[0];
    
    let month = document.getElementsByName('month')[0];
    let day = document.getElementsByName('date')[0];
    let url = `${BASE_URL1}${year.value}-${month.value}-${day.value}${BASE_URL2}${API_KEY}`;
    return url;
}
    

    let getBooks = function(event){ fetch(buildUrl())
        .then((res) => res.json())
        .then((data)=> { 
            parentDivTag.innerHTML = '';
            for(let bookNumber = 0; bookNumber < 5; bookNumber++){
            buildNewBookDiv(data.results.books[bookNumber]);
            }
        })
        .catch((error) => console.log(error));
        event.preventDefault();
    };
        

  function buildNewBookDiv(book){
    let newBook = document.createElement('div');
    newBook.classList.add('borderShadow');
    let title = document.createElement('p');
    title.classList.add('title');
    title.innerHTML = book.title;

    let author = document.createElement('p');
    author.classList.add('my-3');
    author.innerHTML = book.author;
    
    let description = document.createElement('p');
    description.classList.add('description');
    description.innerHTML = book.description;

    let link = document.createElement('a');
    link.innerHTML = 'Buy at...';
    link.href = book.amazon_product_url;

    newBook.appendChild(title);
    newBook.appendChild(author);
    newBook.appendChild(description);
    
    newBook.appendChild(link);
    parentDivTag.appendChild(newBook);
  }
