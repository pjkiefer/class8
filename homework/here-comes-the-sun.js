let h1 = document.getElementsByTagName('h1')[0];

let backGroundColor = {
    red:0,
    green:0,
    blue:0
};
let h1FontColor = {
    red:255,
    green:255,
    blue:255
};
let updateColor = function(){
    backGroundColor.red++;
    backGroundColor.green++;
    backGroundColor.blue++;

    h1FontColor.red--;
    h1FontColor.green--;
    h1FontColor.blue--;
};
let changeColor = function(){
    document.body.style.backgroundColor = `rgb(${backGroundColor.red},${backGroundColor.green},${backGroundColor.blue})`;
    h1.style.color = `rgb(${h1FontColor.red},${h1FontColor.green},${h1FontColor.blue})`;
    updateColor();
    if(backGroundColor.red < 255){
        console.log(`Background color = ${backGroundColor.blue}`);
        window.requestAnimationFrame(changeColor);
    }
    else{
        console.log("Animation loop finished...");
    }
};
window.requestAnimationFrame(changeColor);
