window.onload = function(){
    
};

let promise = new Promise(function(resolve,reject){
    setTimeout(function(){
        let temp = Math.random();
    
        if(temp > 0.5){
            resolve(console.log(`success - ${temp}`));
        }
        else{
            reject(Error(console.log(`failed - ${temp}`)));
        }
}, 2000);
});
promise.then(function(temp){
    console.log('complete - success');
},function(err){
    console.log('complete - failed');
});


